﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    public partial class Form1 : CuPForm
    {
        public Form1()
        {
            InitializeComponent();
            PacketMonitor.All((packet, mode) =>
            {
                Color color = Color.Green;
                if (mode == PacketMode.Send) color = Color.Red;
                else if (mode == PacketMode.AddInSend) color = Color.DarkViolet;
                else if (mode == PacketMode.AddInReceive) color = Color.DarkOrange;
                AppendText(packet + "\r\n", color);
            });
        }

        public void AppendText(string text, Color color)
        {
            if (IsDisposed) return;
            if (InvokeRequired) Invoke(new MethodInvoker(() => AppendText(text, color)));
            else
            {
                richTextBox1.SelectionStart = richTextBox1.TextLength;
                richTextBox1.SelectionLength = 0;

                richTextBox1.SelectionColor = color;
                richTextBox1.AppendText(text);
                richTextBox1.SelectionColor = richTextBox1.ForeColor;

                richTextBox1.SelectionStart = richTextBox1.TextLength;
                richTextBox1.ScrollToCaret();
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            PacketMonitor.All(null);
        }
    }
}
