﻿using Cup.Extensibility.Library;
using System.Threading;
using System;

namespace CuPAddIn
{
    class ColoredNames
    {
        public static bool Enabled
        {
            get { return UserSettings.Get() == "1"; }
            set { if (value) Enable(); else Disable(); }
        }

        public static void Init()
        {
            if (Enabled) Enable();
        }

        private static void Enable()
        {
            AuxSWF.Load();
            PacketMonitor.On("j#crl", (packet, mode) =>
            {
                SetRoomColor();
            }, PacketMode.Send);
            PacketMonitor.On("ap", (packet, mode) =>
            {
                Utils.SetTimeout(() =>
                {
                    string[] pack = packet.Split('%');
                    string[] pinfo = pack[4].Split('|');
                    if (pinfo.Length < 4) return;
                    SetColor(pinfo[0]);
                }, 100);
            });
            PacketMonitor.On("upc", (packet, mode) =>
            {
                string[] pack = packet.Split('%');
                SetColor(pack[4]);
            });
            UserSettings.Set("1");
            SetRoomColor();
            Utils.SetTimeout(() => SetRoomColor(), 5000);
        }

        private static void Disable()
        {
            PacketMonitor.Off();
            SetRoomColor(false);
            UserSettings.Set("");
            AuxSWF.Unload();
        }

        private static void SetRoomColor(bool enable = true)
        {
            if (!Enabled) return;
            Core.ExecuteFunction("AuxSWF.setRoomColor", enable);
        }

        private static void SetColor(string pid)
        {
            Core.ExecuteFunction("AuxSWF.setColor", int.Parse(pid));
        }
    }
}
