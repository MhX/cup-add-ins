﻿using System.AddIn;
using Cup.Extensibility;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    [AddIn("CloudPenguin")]
    public class AddInBase : ICuPAddIn
    {
        public static bool started = false;
        public static Form1 form;

        public void OnBoot() { }

        public void OnStart()
        {
            if (started)
            {
                form = new Form1();
                form.ShowDialog();
                form.Dispose();
                form = null;
            }
            else
            {
                started = true;
                ApiPipeline.Hook();
                ColoredNames.Init();
            }
        }

        public void OnExit()
        {
            started = false;
            if (form != null)
            {
                form.Close();
                form.Dispose();
            }
        }
    }
}
