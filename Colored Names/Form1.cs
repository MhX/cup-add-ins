﻿using System;
using System.Windows.Forms;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    public partial class Form1 : CuPForm
    {
        public static Form1 instance;

        public Form1()
        {
            InitializeComponent();
            instance = this;
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            SetButtons();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ColoredNames.Enabled = true;
            SetButtons();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ColoredNames.Enabled = false;
            SetButtons();
        }

        private void SetButtons()
        {
            if (InvokeRequired) Invoke(new MethodInvoker(SetButtons));
            else
            {
                button1.Enabled = !ColoredNames.Enabled;
                button2.Enabled = ColoredNames.Enabled;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            instance = null;
        }
    }
}