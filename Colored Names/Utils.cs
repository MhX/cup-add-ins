﻿using System.Timers;
using System.Collections.Generic;

namespace CuPAddIn
{
    /// <summary>
    /// Collection of auxiliary methods.
    /// </summary>
    class Utils
    {
        /// <summary>
        /// Delegate for SetTimeout.
        /// </summary>
        public delegate void SetTimeoutCallback();
        private static List<Timer> timers = new List<Timer>();

        /// <summary>
        /// Executes a method after a period of time.
        /// </summary>
        /// <param name="stc">Method to execute.</param>
        /// <param name="time">Time to wait.</param>
        public static void SetTimeout(SetTimeoutCallback stc, int time)
        {
            Timer t = new Timer(time);
            t.Elapsed += (s1, e1) =>
            {
                t.Stop();
                timers.Remove(t);
                stc.Invoke();
            };
            timers.Add(t);
            t.Start();
        }
    }
}
