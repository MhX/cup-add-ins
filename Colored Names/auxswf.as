﻿var INTERFACE = _global.getCurrentInterface();
var SHELL = _global.getCurrentShell();
var ENGINE = _global.getCurrentEngine();
var lpid = 'p' + SHELL.getMyPlayerId();

function setRoomColor(enable){
	for(var pid in INTERFACE.nicknames_mc){
		if(enable) setColor(pid, true);
		else {
			INTERFACE.nicknames_mc[pid].name_txt
				.textColor = ((pid == lpid) ? 0xeeeeee : 0x000000);
		}
	}
}

function setColor(id, alreadyHasPid){
	var pid = (alreadyHasPid) ? id : ('p' + id);
	var colorID = ENGINE.my_room_movieclips.room_mc[pid].colour_id;
	var tColor = SHELL.player_colours[colorID];
	INTERFACE.nicknames_mc[pid].name_txt.textColor = tColor;
}
