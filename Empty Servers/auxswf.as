﻿import com.clubpenguin.login.views.*;
var SHELL = _global.getCurrentShell();
var gc = SHELL.GLOBAL_CRUMBS, gcs = gc.servers;
var vm = SHELL.LOGIN_HOLDER.viewManager;

if(!gc.CuP_resetServers){
	gc.CuP_resetServers = {};
	for(var i in gcs){
		gc.CuP_resetServers[i] = gcs[i].population;
	}
}
var gcc = gc.CuP_resetServers;

function setBars(v){
	for(var i in gcs){
		gcs[i].population = v;
	}
	reloadServers();
}

function resetBars(){
	if(!gcc) return;
	
	for(var i in gcc){
		gcs[i].population = gcc[i];
	}
	reloadServers();
}

function reloadServers(){
	var ws = vm.getView(WorldSelection);
	var ms = vm.getView(MoreServers);
	
	ws.chooseServerScreen();
	if(ms.isActive()) ms.setupMoreServers();
}