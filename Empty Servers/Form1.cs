﻿using Cup.Extensibility.Library;
using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace CuPAddIn
{
    public partial class Form1 : CuPForm
    {
        [DllImport("USER32", EntryPoint = "GetSystemMenu", CallingConvention = CallingConvention.Winapi)]
        private static extern IntPtr GetSystemMenu(IntPtr WindowHandle, int bReset);
        [DllImport("USER32", EntryPoint = "EnableMenuItem", CallingConvention = CallingConvention.Winapi)]
        private static extern bool EnableMenuItem(IntPtr hMenu, int uIDEnableItem, int uEnable);
        private bool CloseButton
        {
            set
            {
                Invoke(new MethodInvoker(() =>
                    EnableMenuItem(GetSystemMenu(Handle, 0), 0xf060, (value) ? 0 : 1)
                ));
            }
        }
        private bool mouseDown = false;

        public Form1()
        {
            InitializeComponent();
            ApiPipeline.Hook();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormState(false);
            int v = trackBar1.Value + 1;
            if (v == 6) v = 7;
            Core.ExecuteFunction("AuxSWF.setBars", v);
            FormState(true);
            button1.Focus();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormState(false);
            Core.ExecuteFunction("AuxSWF.resetBars");
            FormState(true);
            button2.Focus();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Hide();
            e.Cancel = true;
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            if (!AuxSWF.IsLoaded) AuxSWF.Load();
        }

        private void trackBar1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseDown = true;
                trackBar1_MouseMove(sender, e);
            }
        }

        private void trackBar1_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                trackBar1.Value = Convert.ToInt32(((double)e.X /
                    (double)trackBar1.Width) * (trackBar1.Maximum -
                    trackBar1.Minimum));
            }
        }

        private void trackBar1_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void FormState(bool state)
        {
            CloseButton = state;
            trackBar1.Enabled = state;
            button1.Enabled = state;
            button2.Enabled = state;
        }
    }
}
