﻿class com.clubpenguin.ui.views.AbstractView extends MovieClip implements com.clubpenguin.ui.views.IView
{
	var _shell, viewManager, _visible;
	function AbstractView () {
		super();
	}
	function addEventListener(type, func, scope) {
		return(false);
	}
	function removeEventListener(type, func, scope) {
		return(false);
	}
	function updateListeners(type, event) {
		return(false);
	}
	function dispatchEvent(event) {
		return(false);
	}
	function setLanguageAbbr(abbr) {
	}
	function setShell(target) {
		_shell = target;
	}
	function setViewManager(manager) {
		viewManager = manager;
	}
	function getViewManager() {
		return(viewManager);
	}
	function hide() {
		_visible = false;
	}
	function show() {
		_visible = true;
	}
}
