﻿class com.clubpenguin.login.views.WorldSelection extends com.clubpenguin.login.views.ServerSelectionView
{
	var _backgroundClip, bg_mc, _suggestedWorldClips, world_00_mc, world_01_mc, world_02_mc, world_03_mc, world_04_mc, _redemptionBadgeClip, redemption_mc, _selectWorldField, select_world_txt, _buddiesField, buddies_txt, _numPenguinsOnlineField, amount_peng_txt, _ultimateSafeChatField, ultimate_safe_txt, _moreServersClip, more_servers_btn, _trackerAS2, _contentTracked, _localizedTrackingName, _languageAbbreviation, viewManager, _visible, _shell, hide, _selectedWorldID;
	function WorldSelection () {
		super();
	}
	function setLanguageAbbr(abbr) {
	}
	function show() {
	}
	function showMoreServersOnRelease() {
	}
	function setupWorldSelection() {
	}
	function setRedemptionForWorldSelection() {
	}
	function onRedemptionPress() {
	}
	function translateWorldSelection() {
	}
	function chooseServerScreen() {
	}
	function showReturnUser(serverList, lastWorldId) {
	}
	function setupWorldClips(data) {
	}
	function onRosterUpdated(numBuddies) {
	}
	function rollOverWorldSelection(worldClip) {
	}
	function rollOutWorldSelection(worldClip) {
	}
	function connectToWorld(worldClip) {
	}
	static function debugTrace(msg) {
	}
	static var LINKAGE_ID = "com.clubpenguin.login.views.WorldSelection";
	static var FULL_POPULATION = 7;
	static var FIVE_BARS_HIGH = 6;
	static var FIVE_BARS = 5;
	static var FOUR_BARS = 4;
	static var THREE_BARS = 3;
	static var TWO_BARS = 2;
	static var ONE_BARS = 1;
	static var TARGET_POPULATION = 3;
	static var SUGGESTED_WORLD_DISPLAY_COUNT = 5;
	static var TRACKING_NAME = "select_server_";
	static var _debugTracesActive = true;
	var worldSelectLogSent = false;
}