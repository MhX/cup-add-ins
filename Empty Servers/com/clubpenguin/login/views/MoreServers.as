class com.clubpenguin.login.views.MoreServers extends com.clubpenguin.login.views.ServerSelectionView
{
	var _moreServersCloseClip, close_btn, _moreServersListClip, list_mc, _pleaseSelectField, select_world_txt, down_btn, up_btn, _visible, viewManager, _shell;
	function MoreServers () {
	}
	function isActive() {
	}
	function show() {
	}
	function hideMoreServers() {
	}
	function translateMoreServers() {
	}
	function moreServerPageUp() {
	}
	function moreServerPageDown() {
	}
	function getMaxMoreServerPages() {
	}
	function setupMoreServers() {
	}
	function createServer(server, i) {
	}
	function onRosterUpdated(numBuddies) {
	}
	function removeServer(i) {
	}
	function rollOverMoreServer(mcServer) {
	}
	function rollOffMoreServer(mcServer) {
	}
	function connectToWorld(clip) {
	}
	static function debugTrace(msg) {
	}
	static var LINKAGE_ID = "com.clubpenguin.login.views.MoreServers";
	static var SERVERS_PER_PAGE = 22;
	static var _debugTracesActive = false;
	var _currentPage = 0;
	var _sortOption = "name";
	var _moreServersActive = false;
}