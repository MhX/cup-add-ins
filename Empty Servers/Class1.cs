﻿using Cup.Extensibility;
using Cup.Extensibility.Library;
using System.AddIn;

namespace CuPAddIn
{
    [AddIn("CloudPenguin")]
    public class AddInBase : ICuPAddIn
    {
        public static bool started = false;
        public static Form1 form;

        public void OnBoot() { }

        public void OnStart()
        {
            if (started)
            {
                form.ShowDialog();
            }
            else
            {
                started = true;
                form = new Form1();
            }
        }

        public void OnExit()
        {
            started = false;
            AuxSWF.Unload();
            if (form != null)
            {
                form.Close();
                form.Dispose();
            }
        }
    }
}