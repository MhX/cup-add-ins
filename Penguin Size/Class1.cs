﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.AddIn;
using Cup.Extensibility;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    [AddIn("CloudPenguin")]
    public class AddInBase : ICuPAddIn
    {
        public static double[] Widths = new double[5] { 8.5, 17, 34, 100, 300 };
        public static double[] Heights = new double[5] { 10.3625, 20.725, 44, 100, 300 };
        public static int currentSize = 2;

        public static bool started = false;
        public static bool opened = false;
        public static Form1 form;

        public void OnBoot() { }

        public void OnStart()
        {
            if (started && !opened)
            {
                opened = true;
                using (form = new Form1())
                {
                    form.ShowDialog();
                }
                form = null;
                opened = false;
            }
            else
            {
                started = true;
                ApiPipeline.Hook();
                AuxServer.OnPacketReceived = (type, packet) =>
                {
                    if (type == AuxServer.Type.SendRoom)
                    {
                        string[] pack = packet.Split('|');
                        if (pack.Length != 2) return;

                        int ID, index;
                        if (!int.TryParse(pack[0], out ID)) return;
                        if (!int.TryParse(pack[1], out index)) return;
                        if (index < 0 || index > 4) return;

                        Core.SetVarAsync("ENGINE.my_room_movieclips.room_mc.p" +
                            pack[0] + "._width", Widths[index], null);
                        Core.SetVarAsync("ENGINE.my_room_movieclips.room_mc.p" +
                            pack[0] + "._height", Heights[index], null);
                    }
                };
                PacketMonitor.On("ap", (packet, mode) => OnUserJoin(
                    packet.Split('%').Skip(4).ToArray()));
                PacketMonitor.On("jr", (packet, mode) =>
                {
                    currentSize = 2;
                    if (opened) form.UpdateButtons();
                    OnUserJoin(packet.Split('%').Skip(5).ToArray());
                });
            }
        }

        public void OnExit()
        {
            started = false;
            opened = false;
            AuxServer.OnPacketReceived = null;
            if (form != null)
            {
                form.Close();
                form.Dispose();
            }
        }

        private void OnUserJoin(string[] users)
        {
            if (users.Length == 0 || currentSize == 2) return;
            bool found = false;
            foreach (string user in users)
            {
                if (user.Length == 0) continue;
                string[] uinfo = user.Split('|');
                if (AuxServer.IsCuPUser(uinfo[0]))
                {
                    found = true;
                    break;
                }
            }
            if (found) AuxServer.SendRoom(LocalPlayer.Id + "|" + currentSize);
        }
    }
}