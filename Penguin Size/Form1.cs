﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cup.Extensibility.Library;

namespace CuPAddIn
{
    public partial class Form1 : CuPForm
    {
        public Form1()
        {
            InitializeComponent();
            UpdateButtons();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Core.SetVarAsync("ENGINE.my_room_movieclips.room_mc.p" + 
                LocalPlayer.Id + "._width", AddInBase.Widths[0], null);
            Core.SetVarAsync("ENGINE.my_room_movieclips.room_mc.p" +
                LocalPlayer.Id + "._height", AddInBase.Heights[0], null);
            AuxServer.SendRoom(LocalPlayer.Id + "|0");
            AddInBase.currentSize = 0;
            UpdateButtons();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Core.SetVarAsync("ENGINE.my_room_movieclips.room_mc.p" +
                LocalPlayer.Id + "._width", AddInBase.Widths[1], null);
            Core.SetVarAsync("ENGINE.my_room_movieclips.room_mc.p" +
                LocalPlayer.Id + "._height", AddInBase.Heights[1], null);
            AuxServer.SendRoom(LocalPlayer.Id + "|1");
            AddInBase.currentSize = 1;
            UpdateButtons();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Core.SetVarAsync("ENGINE.my_room_movieclips.room_mc.p" +
                LocalPlayer.Id + "._width", AddInBase.Widths[2], null);
            Core.SetVarAsync("ENGINE.my_room_movieclips.room_mc.p" +
                LocalPlayer.Id + "._height", AddInBase.Heights[2], null);
            AuxServer.SendRoom(LocalPlayer.Id + "|2");
            AddInBase.currentSize = 2;
            UpdateButtons();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Core.SetVarAsync("ENGINE.my_room_movieclips.room_mc.p" +
                LocalPlayer.Id + "._width", AddInBase.Widths[3], null);
            Core.SetVarAsync("ENGINE.my_room_movieclips.room_mc.p" +
                LocalPlayer.Id + "._height", AddInBase.Heights[3], null);
            AuxServer.SendRoom(LocalPlayer.Id + "|3");
            AddInBase.currentSize = 3;
            UpdateButtons();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Core.SetVarAsync("ENGINE.my_room_movieclips.room_mc.p" +
                LocalPlayer.Id + "._width", AddInBase.Widths[4], null);
            Core.SetVarAsync("ENGINE.my_room_movieclips.room_mc.p" +
                LocalPlayer.Id + "._height", AddInBase.Heights[4], null);
            AuxServer.SendRoom(LocalPlayer.Id + "|4");
            AddInBase.currentSize = 4;
            UpdateButtons();
        }

        public void UpdateButtons()
        {
            if (InvokeRequired) Invoke(new MethodInvoker(UpdateButtons));
            else
            {
                button1.Enabled = AddInBase.currentSize != 0;
                button2.Enabled = AddInBase.currentSize != 1;
                button3.Enabled = AddInBase.currentSize != 2;
                button4.Enabled = AddInBase.currentSize != 3;
                button5.Enabled = AddInBase.currentSize != 4;
            }
        }
    }
}